
//
//  ViewController.swift
//  maqamat
//
//  Created by AhmeDroid on 3/18/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        UserDefaults.standard.set(true, forKey:"launchedOnce")
        
        //["DINNextLTW23-Medium", "DINNextLTW23-Regular", "DINNextLTW23-Bold"]
        
        if let font = UIFont(name: "DINNextLTW23-Medium", size: 11) {
            UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: font], for: .normal)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(openOrganizeTripView),
                                               name: NotificationNames.OrganizeTripWasRequested, object: nil)
    }
    
    var initialUserContentTab:Int = 0
    func openOrganizeTripView() -> Void {
        
        self.initialUserContentTab = 1
        self.selectedIndex = 2
    }
}
