//
//  ProvinceCardCell.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/26/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class ProvinceCardCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var container: UIView!
    
    var whiteViews = [UIView]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.clipsToBounds = false
        self.imageView.layer.masksToBounds = true
        self.imageView.layer.cornerRadius = 4
        self.container.layer.masksToBounds = false
        self.container.layer.cornerRadius = 4
        self.container.layer.shadowColor = UIColor.black.cgColor
        
        for i in 0 ..< 3 {
            
            let view = UIView(frame: self.container.bounds)
            view.backgroundColor = UIColor.white
            view.layer.cornerRadius = 4
            view.isUserInteractionEnabled = false
            
            self.insertSubview(view, at: i)
            self.whiteViews.append(view)
        }
    }
    
    var arranged:Bool = false
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !arranged {
            self.collapse(animated: false)
            arranged = true
        }
    }
    
    func collapse(animated:Bool = true) -> Void {
        
        if animated {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.container.alpha = 0.4
                self.container.transform = CGAffineTransform.identity
                self.container.layer.shadowRadius = 2
                self.container.layer.shadowOpacity = 0.4
                self.container.layer.shadowOffset = CGSize(width: 0, height: 2)
                
                let fr = self.transformAroundCenter(self.container.frame, transform: CGAffineTransform.identity)
                
                self.whiteViews.forEach({ (view) in
                    view.alpha = 0.0
                    view.frame = fr
                })
                
            }, completion: { (finish) in
                
                self.whiteViews.forEach({ (view) in
                    view.isHidden = true
                })
            })
            
        } else {
            
            self.container.alpha = 0.4
            self.container.transform = CGAffineTransform.identity
            self.container.layer.shadowRadius = 2
            self.container.layer.shadowOpacity = 0.4
            self.container.layer.shadowOffset = CGSize(width: 0, height: 2)
            
            self.whiteViews.forEach({ (view) in
                view.isHidden = true
                view.alpha = 0.0
                view.frame = self.container.frame
            })
            
        }
    }
    
    private func transformAroundCenter(_ frame:CGRect, transform at:CGAffineTransform ) -> CGRect {
        return frame.applying(CGAffineTransform(translationX: -frame.width/2.0, y: -frame.height/2.0))
            .applying(at)
            .applying(CGAffineTransform(translationX: frame.width/2.0, y: frame.height/2.0))
    }
    
    func expand(animated:Bool = true) -> Void {
        
        self.whiteViews.forEach({ (view) in
            view.isHidden = false
        })
        
        let at = CGAffineTransform(scaleX: 1.4, y: 1.4)
        let tr = self.transformAroundCenter(self.container.frame, transform: at)
        
        let applyProps = {
            
            self.container.alpha = 1.0
            self.container.transform = at
            self.container.layer.shadowOffset = CGSize(width: 0, height: 40)
            self.container.layer.shadowRadius = 10
            self.container.layer.shadowOpacity = 0.4
            
            
            let c = CGFloat(self.whiteViews.count)
            for i in 0 ..< self.whiteViews.count {
                
                let view = self.whiteViews[i]
                let k = CGFloat(i)
                
                let dx = 7 * (c - k)
                let dy = 7 * (c - k)
                let vr = tr
                    .insetBy(dx: dx, dy: dy)
                    .offsetBy(dx: 0, dy: 2 * dy + (c - k))
                
                view.alpha = 0.3
                view.frame = vr
            }
        }
        
        if animated {
            
            UIView.animate(withDuration: 0.3, animations: applyProps)
            
        } else {
            
            applyProps()
        }
    }
}


