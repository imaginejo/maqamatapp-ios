//
//  SearchViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/27/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SVProgressHUD
import CoreLocation

class SearchViewController: UIViewController, UITextFieldDelegate {
    
    class func presentOn(_ viewController:UIViewController ) -> Void {
        
        if let searchVC = viewController.storyboard?.instantiateViewController(withIdentifier: "searchVC") as? SearchViewController {
            
            viewController.present(searchVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableBelowConst: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var fieldContainer: UIView!
    
    var results:[SearchResult] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fieldContainer.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0, height: 1), radius: 3.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textField.becomeFirstResponder()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
    }
    
    var timer:Timer?
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector( checkTextFieldForContent ), userInfo: nil, repeats: false)
        return true
    }
    
    func checkTextFieldForContent() -> Void {
        
        let phrase = (self.textField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        if phrase.isEmpty {
            
            self.results.removeAll()
            self.tableView.reloadData()
            
        } else {
            
            self.requestMqamatSearch(phrase)
        }
        
        self.timer = nil
    }
    
    func requestMqamatSearch(_ phrase:String ) -> Void {
        
        if let keyword = phrase.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            
            self.activityIndicator.startAnimating()
            
            Alamofire.request(APIEndpoint.maqamat(province: nil, keyword: keyword, location: nil))
                .responseSwiftyJSON(completionHandler: { (response) in
                    
                    self.activityIndicator.stopAnimating()
                    
                    if let json = response.result.value {
                        
                        if let items = json["items"].array {
                            
                            self.results.removeAll()
                            for item in items {
                                
                                if let maqam = Maqam.parseMaqamData(item) {
                                    
                                    self.results.append( SearchResult(maqam: maqam) )
                                }
                            }
                            
                            self.tableView.reloadData()
                        }
                        
                    } else if let err = response.error {
                        
                        showErrorMessage(err.localizedDescription)
                    }
            })
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            self.tableBelowConst.constant = keyboardFrame.cgRectValue.height
        }
    }
    
    
    deinit {
        
        self.results.forEach { (result) in
            result.image = nil
        }
        
        self.results.removeAll()
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "searchResultCell", for: indexPath)
        
        let result = self.results[indexPath.row]
        
        cell.textLabel?.text = result.maqam.name
        cell.detailTextLabel?.text = result.maqam.province.name
        cell.imageView?.image = nil
        
        result.loadImage { (image) in
            
            cell.imageView?.image = image
            cell.layoutSubviews()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let result = self.results[indexPath.row]
        loadThenShowMaqamDetails(result.maqam.id)
    }
}


class SearchResult {
    
    var maqam:Maqam
    var image:UIImage?
    
    init(maqam:Maqam) {
        self.maqam = maqam
    }
    
    func loadImage(completion:@escaping (UIImage?) -> Void) -> Void {
        
        if self.image != nil {
            
            completion(self.image)
        } else {
            
            if let imgUrl = self.maqam.imageUrl {
                
                Alamofire.request(imgUrl).responseImage(completionHandler: { (result) in
                    
                    let image = result.result.value?
                        .af_imageAspectScaled(toFill: CGSize(width: 34, height: 34))
                        .af_imageRounded(withCornerRadius: 17)
                    
                    self.image = image
                    completion(image)
                })
            }
        }
    }
}

func loadThenShowMaqamDetails(_ maqamId:Int) -> Void {
    
    SVProgressHUD.setDefaultMaskType(.gradient)
    SVProgressHUD.setDefaultStyle(.light)
    SVProgressHUD.show()
    
    Alamofire.request(APIEndpoint.maqamDetails(maqamId: maqamId))
        .responseSwiftyJSON(completionHandler: { (response) in
            
            SVProgressHUD.dismiss()
            
            if let json = response.result.value {
                
                let details = json["details"]
                if let id = details["id"].int,
                    let name = details["name"].string,
                    let temperature = details["temperature"].int,
                    let weatherId = details["weather"].int,
                    let typeId = details["type"].int,
                    let type = CharacterType(rawValue: typeId),
                    let provinceId = details["province"].int,
                    let province = Province.province(forId: provinceId),
                    let latitude = details["latitude"].double,
                    let longitude = details["longitude"].double {
                    
                    let coords = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let info = MaqamInfo(id: id,
                                         name: name,
                                         temperature: temperature,
                                         weather: WeatherState.state(forId: weatherId),
                                         type: type, province: province, coords: coords)
                    
                    info.aboutCharacter = details["about_character"].string
                    info.aboutMaqam = details["about_maqam"].string
                    info.openDays = details["open_days"].string
                    info.openHours = details["open_hours"].string
                    info.phoneNumber = details["phone_number"].string
                    
                    if let photos = details["photos"].array {
                        
                        info.photoUrls = photos.map({ (json) -> String in
                            return json.string ?? "N/A"
                        }).filter({ (string) -> Bool in
                            return string != "N/A"
                        })
                    }
                    
                    
                    if let topVC = UIApplication.topViewController(),
                        let maqamDetailsVC = topVC.storyboard?.instantiateViewController(withIdentifier: "maqamDetailsVC") as? MaqamDetailsViewController {
                        
                        maqamDetailsVC.maqamInfo = info
                        topVC.present(maqamDetailsVC, animated: true, completion: nil)
                    }
                }
                
            } else if let err = response.error {
                
                showErrorMessage(err.localizedDescription)
            }
        })
}
