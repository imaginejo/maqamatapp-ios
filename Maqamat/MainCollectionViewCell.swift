//
//  MainCollectionViewCell.swift
//  maqamat
//
//  Created by AhmeDroid on 3/21/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import Alamofire

class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var governorateLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    private var lowerMask:CAShapeLayer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageView.layer.cornerRadius = 3
        self.imageView.layer.masksToBounds = true
        
        self.clipsToBounds = false
        self.backgroundColor = UIColor.clear
        self.container.layer.cornerRadius = 4
        self.container.applyDarkShadow(opacity: 0.25, offset: CGSize(width: 0, height: 1), radius: 2)
        
        let mask = CAShapeLayer()
        
        self.lowerMask = mask
        self.lowerView.layer.mask = mask
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.lowerMask.path = UIBezierPath(roundedRect: self.lowerView.bounds,
                                           byRoundingCorners: [.bottomRight, .bottomLeft],
                                           cornerRadii: CGSize(width: 4, height: 4) ).cgPath
    }
    
    @IBAction func openAboutMaqam(_ sender: Any) {
        
        loadThenShowMaqamDetails(self.currentDisp.maqam.id)
    }
    
    @IBAction func openMaqamDirections(_ sender: Any) {
        
        GoogleMapViewController.present(forActivity: self.currentDisp)
    }
    
    var imgReq:DataRequest?
    var currentDisp:MainMaqamDisp!
    func showMaqam(_ disp:MainMaqamDisp ) -> Void {
        
        let maqam = disp.maqam
        self.currentDisp = disp
        self.distanceLabel.text = maqam.distanceString
        self.nameLabel.text = maqam.name
        self.governorateLabel.text = maqam.province.name
        
        self.imgReq?.cancel()
        
        if let image = disp.image {
            
            self.imageView.image = image
        } else if let imgPath = maqam.imageUrl {
            
            self.imgReq = Alamofire.request(imgPath).responseImage(completionHandler: { (res) in
                
                if let image = res.result.value {
                    
                    let photo = image.af_imageAspectScaled(toFill: self.imageView.frame.size)
                    self.currentDisp?.image = photo
                    self.imageView?.image = photo
                }
            })
        }
    }
}
