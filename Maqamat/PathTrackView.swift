//
//  PathTrackView.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class PathTrackView: UIView {
    
    var greenBar = UIView()
    var grayBar = UIView()
    
    private let greenColor = UIColor(valueRed: 90, green: 179, blue: 47, alpha: 1)
    private let grayColor = UIColor(white: 0.91, alpha: 1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() -> Void {
        
        self.greenBar.backgroundColor = greenColor
        self.grayBar.backgroundColor = grayColor
        
        self.addSubview(self.grayBar)
        self.addSubview(self.greenBar)
    }
    
    var progress:Int = 0 {
        didSet {
            self.setNeedsLayout()
            self.setNeedsDisplay()
        }
    }
    
    var numOfDestinations:Int = 0 {
        didSet {
            self.setNeedsLayout()
            self.setNeedsDisplay()
        }
    }
    
    var shapes:[CAShapeLayer] = []
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.shapes.forEach { (shp) in
            shp.removeFromSuperlayer()
        }
        
        let f = self.bounds
        let dh = f.height / CGFloat( self.numOfDestinations - 1 )
        let fh = (self.progress > 0) ? dh * CGFloat(self.progress - 1) : 0
        
        self.grayBar.frame = CGRect(x: f.midX - 1.5, y: 0, width: 3, height: f.height)
        self.greenBar.frame = CGRect(x: f.midX - 1, y: 0, width: 2, height: fh)
        
        let ph = 16 as CGFloat
        var pf = CGRect(x: f.midX - ph / 2.0, y: -ph / 2.0, width: ph, height: ph)
        
        if self.numOfDestinations > 0 {
            
            for i in 1 ... self.numOfDestinations {
                
                let layer = CAShapeLayer()
                layer.frame = pf
                layer.path = UIBezierPath(ovalIn: CGRect(origin: CGPoint.zero, size: pf.size)).cgPath
                layer.fillColor = i > self.progress
                    ? grayColor.cgColor
                    : greenColor.withAlphaComponent(0.3).cgColor
                
                
                let sf = pf.insetBy(dx: ph * 0.24, dy: ph * 0.24)
                let solid = CAShapeLayer()
                solid.frame = sf
                solid.path = UIBezierPath(ovalIn: CGRect(origin: CGPoint.zero, size: sf.size)).cgPath
                solid.fillColor = i > self.progress
                    ? UIColor(valueRed: 91, green: 126, blue: 156, alpha: 1).cgColor
                    : greenColor.cgColor
                
                
                pf = pf.offsetBy(dx: 0, dy: dh)
                self.layer.addSublayer(layer)
                self.layer.addSublayer(solid)
                self.shapes.append(layer)
                self.shapes.append(solid)
            }
        }
    }
    
}
