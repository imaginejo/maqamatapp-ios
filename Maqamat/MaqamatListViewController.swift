//
//  ProvinceMaqamatViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/26/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class MaqamatListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var currentProvince:Province?
    var currentList:[Maqam]?
    var currentTitle:String?
    
    var maqamatList:[Maqam] = []
    private var refreshControl:UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingIndicator.stopAnimating()
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        
        if let province = self.currentProvince {
            
            self.titleLabel.text = province.name
            self.fetchMaqamatItems( province )
            
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector( refetchProvinceMaqamatList ), for: .valueChanged)
            refreshControl?.tintColor = UIColor.gray
            refreshControl?.attributedTitle = NSAttributedString(string: "إعادة التحميل",
                                                                attributes: [NSForegroundColorAttributeName: UIColor.gray])
            
            // Add Refresh Control to Table View
            if #available(iOS 10.0, *) {
                self.tableView.refreshControl = refreshControl
            } else {
                self.tableView.addSubview(refreshControl!)
            }
            
        } else if let maqamat = self.currentList {
            
            self.titleLabel.text = self.currentTitle
            self.maqamatList = maqamat
            self.tableView.reloadData()
        }
    }
    
    func refetchProvinceMaqamatList() -> Void {
        
        if let province = self.currentProvince {
            self.fetchMaqamatItems( province )
        }
    }
    
    func fetchMaqamatItems(_ province:Province) -> Void {
        
        self.loadingIndicator.startAnimating()
        
        Alamofire.request(APIEndpoint.maqamat(province: province.id, keyword: nil, location: nil))
            .responseSwiftyJSON(completionHandler: { (response) in
                
                self.loadingIndicator.stopAnimating()
                self.refreshControl?.endRefreshing()
                
                if let json = response.result.value {
                    
                    if let items = json["items"].array {
                        
                        self.maqamatList.removeAll()
                        for item in items {
                            
                            if let maqam = Maqam.parseMaqamData(item) {
                                
                                self.maqamatList.append( maqam )
                            }
                        }
                        
                        self.tableView.reloadData()
                    }
                    
                } else if let err = response.error {
                    
                    showErrorMessage(err.localizedDescription)
                }
            })
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.maqamatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "provinceMaqamCell", for: indexPath) as! ProvinceMaqamCell
        
        let maqam = self.maqamatList[indexPath.row]
        cell.nameLabel.text = maqam.name
        cell.governorateLabel.text = maqam.province.name
        cell.distanceLabel.text = maqam.distanceString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let maqam = self.maqamatList[indexPath.row]
        loadThenShowMaqamDetails(maqam.id)
    }
}
