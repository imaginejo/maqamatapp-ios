//
//  FavouriteViewController.swift
//  maqamat
//
//  Created by AhmeDroid on 3/18/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class UserContentViewController: UIViewController, PageViewControllerListener {
    
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var orangeLines: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tabBarVC = self.tabBarController as? MainTabBarController {
            self.showTabContent(atIndex: tabBarVC.initialUserContentTab)
        } else {
            self.showTabContent(atIndex: 0)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector( openOrganizeTripTab ),
                                               name: NotificationNames.OrganizeTripWasRequested,
                                               object: nil)
    }
    
    func openOrganizeTripTab() -> Void {
        
        self.showTabContent(atIndex: 1)
    }
    
    var currentIndex:Int = 0
    func showTabContent(atIndex index:Int) -> Void {
        
        self.currentIndex = index
        self.focusOnTab(atIndex: index)
        self.pageControl?.showPageOfIndex(index)
    }
    
    func focusOnTab(atIndex index:Int) -> Void {
        
        let i:Int = (index == 0) ? 0 : 1;
        let j:Int = (index == 0) ? 1 : 0;
        
        self.orangeLines[i].isHidden = false;
        self.buttons[i].setTitleColor(UIColor.white, for: .normal)
        
        self.orangeLines[j].isHidden = true;
        self.buttons[j].setTitleColor(UIColor(white: 1, alpha: 0.6), for: .normal)
    }
    
    @IBAction func favouritePlacesTapped(_ sender: Any) {
        
        self.showTabContent(atIndex: 0)
    }
    
    @IBAction func organizeYourTripButtonTapped(_ sender: Any) {
        
        self.showTabContent(atIndex: 1)
    }
    
    var pageControl:PageViewController?
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let ident = segue.identifier, ident == "embed_page_controller" {
            
            let storyboard = self.storyboard!
            self.pageControl = segue.destination as? PageViewController
            self.pageControl?.listener = self
            self.pageControl?.isSwipeEnabled = false
            self.pageControl?.initialIndex = self.currentIndex
            self.pageControl?.orderedViewControllers = [
                storyboard.instantiateViewController(withIdentifier: "favoritesVC"),
                storyboard.instantiateViewController(withIdentifier: "organizeVC")
            ]
        }
    }
    
    func pageViewController(_ pageView: PageViewController, didSwitchToIndex index: Int) {
        
        self.focusOnTab(atIndex: index)
    }
}
