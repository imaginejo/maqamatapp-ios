//
//  RatingViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 4/1/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class RatingViewController: UIViewController {
    
    var currentMaqam:MaqamInfo!
    
    class func present(forMaqam maqam:MaqamInfo) {
        
        if let topVC = UIApplication.topViewController(),
            let ratingVC = topVC.storyboard?.instantiateViewController(withIdentifier: "ratingVC") as? RatingViewController {
            
            ratingVC.currentMaqam = maqam
            
            topVC.present(ratingVC, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet var starButtons: [UIButton]!
    
    var baseRect:CGRect = CGRect.zero
    
    @IBOutlet weak var const_height: NSLayoutConstraint!
    @IBOutlet weak var const_bottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adaptTextView()
    }
    
    @IBAction func containerWasTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        
        self.adaptTextView()
        self.const_height.constant = self.baseRect.height
        self.const_bottom.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.baseRect == CGRect.zero {
            self.baseRect = self.container.frame
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
    }
    
    @IBOutlet weak var placeholderLabel: UILabel!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            
            let kf = keyboardFrame.cgRectValue
            self.const_bottom.constant = kf.size.height
            self.const_height.constant = min(kf.minY - 25, self.baseRect.height)
            self.placeholderLabel.isHidden = true
            self.commentTextView.textColor = UIColor.black
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func adaptTextView() -> Void {
        
        let text = self.commentTextView.text ?? ""
        self.commentTextView.textColor = text.isEmpty ? UIColor.gray : UIColor.black
        self.placeholderLabel.isHidden = !text.isEmpty
    }
    
    private let selImage = UIImage(named: "ic_star_36pt")
    private let brdImage = UIImage(named: "ic_star_border_36dp")
    
    var selectedValue:Int?
    @IBAction func starButtonWasTapped(_ sender: Any) {
        
        let selected = sender as! UIButton
        let value = selected.tag
        
        let gold = UIColor(valueRed: 245, green: 159, blue: 2, alpha: 1)
        let gray = UIColor(valueRed: 209, green: 214, blue: 220, alpha: 1)
        
        for i in 0 ..< self.starButtons.count {
            self.starButtons[i].setImage( i < value ? selImage: brdImage , for: .normal)
            self.starButtons[i].tintColor = i < value ? gold : gray
        }
        
        self.selectedValue = value
    }
    
    @IBAction func viewWasTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitRating(_ sender: Any) {
        
        if let rating = self.selectedValue {
            
            SVProgressHUD.setMaximumDismissTimeInterval(1)
            SVProgressHUD.show()
        
            Alamofire.request(APIEndpoint.rating(
                maqamId: self.currentMaqam.id,
                rating: rating,
                comment: self.commentTextView.text ?? "")).responseData(completionHandler: { (response) in
                            
                            SVProgressHUD.dismiss()
                            
                            if response.result.isSuccess {
                                SVProgressHUD.showSuccess(withStatus: "تم التقييم بنجاح")
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                                    self.dismiss(animated: true, completion: nil)
                                })
                                
                            } else {
                                
                                SVProgressHUD.showSuccess(withStatus: "تعذر التقييم. من فضلك حاول في وقت لاحق")
                            }
                          })
        } else {
            
            showErrorMessage("الرجاء اختيار التقييم")
        }
    }
}
