//
//  VisitsManager.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/27/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import CoreLocation

class VisitsManager: NSObject, CLLocationManagerDelegate {
    
    static var shared = VisitsManager()
    
    var locationManager:CLLocationManager
    private override init() {
        
        self.locationManager = CLLocationManager()
        
        super.init()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        print("Location Change Authorization !")
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            
            manager.requestAlwaysAuthorization()
            
        } else if status == .authorizedAlways {
            
            self.locationManager.startUpdatingLocation()
            
        } else if status == .authorizedWhenInUse {
            
            manager.requestAlwaysAuthorization()
            
        } else if status == .denied || status == .restricted {
            
            showErrorMessage("Access to location service was denied by you. Please give access to location services to enjoy the full of features of the app.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("Error in location service: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        let regId = region.identifier
        if let maqamId = Int(regId) {
            print("Did Enter Region for Maqam (\(maqamId)): \(Date())")
            self.visitsHistroy[maqamId] = Date()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        let regId = region.identifier
        if let maqamId = Int(regId) {
            
            print("Did Exit Region for Maqam (\(maqamId)): \(Date())")
            
            if let enterDate = self.visitsHistroy[maqamId] {
                
                let duration = Date().timeIntervalSince(enterDate)
                let minutes = duration / 60
                
                print("Visit duration for Maqam (\(maqamId)): \(minutes) minutes")
                
                if minutes > 15 {
                    
                    UserAppData.setMaqamVisited(maqamId)
                }
            }
        }
    }
    
    private var visitsHistroy = [Int:Date]()
    func updateMonitoredRegions() -> Void {
        
        self.locationManager.monitoredRegions.forEach { (region) in
            self.locationManager.stopMonitoring(for: region)
        }
        
        var allIds = [Int]()
        let tripsItems = UserAppData.tripsItems()
        tripsItems.forEach { (maqam) in
            
            let region = CLCircularRegion(center: maqam.coords, radius: 500, identifier: "\(maqam.id)")
            self.locationManager.startMonitoring(for: region)
            allIds.append(maqam.id)
        }
        
        var visits = [Int:Date]()
        self.visitsHistroy.forEach { (pair) in
           
            if allIds.contains(pair.key) {
                visits[pair.key] = pair.value
            }
        }
        
        self.visitsHistroy = visits
    }
}
