//
//  AppDelegate.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/22/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMaps
import GooglePlaces
import Alamofire
import Firebase
import FirebaseMessaging
import FirebaseInstanceID

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Province.reloadMaqamatCounts(completion: nil)
        GMSServices.provideAPIKey(APIKeys.Google)
        GMSPlacesClient.provideAPIKey(APIKeys.Google)
        
        FirebaseApp.configure()
        self.requestRemoteNotificationRegiseration(application)
        
        return true
    }
    
    func requestRemoteNotificationRegiseration(_ application:UIApplication ) -> Void {
        
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler( UNNotificationPresentationOptions.alert.union(.sound) );
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = String(format: "%@", deviceToken as CVarArg)
            .trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
            .replacingOccurrences(of: " ", with: "")
        
        UserDefaults.standard.set(token, forKey: "deviceToken")
        Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)
        
        if let fcmToken = Messaging.messaging().fcmToken {
            UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        }
        
        print("Device token was recieved: \( token )")
        print("Firebase Token: \( Messaging.messaging().fcmToken ?? "N/A" )")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

class UserAppData {
    
    private class func addMaqamToList(_ maqam:Maqam, key:String) -> Void {
        
        var tripsArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] ?? [[String:Any]]()
        
        let itemIndex = tripsArray.index(where: { (item) -> Bool in
            let id = item["id"] as! Int
            return id == maqam.id
        })
        
        if let i = itemIndex {
            
            tripsArray[i] = maqam.dictionary()
            
        } else {
            
            tripsArray.append(maqam.dictionary())
        }
        
        UserDefaults.standard.set(tripsArray, forKey: key)
    }
    
    private class func removeMaqamFromList(_ maqamId:Int, key:String) -> Void {
        
        var tripsArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] ?? [[String:Any]]()
        
        if let index = tripsArray.index(where: { (item) -> Bool in
            let id = item["id"] as! Int
            return id == maqamId
        }) {
            
            tripsArray.remove(at: index)
        }
        
        UserDefaults.standard.set(tripsArray, forKey: key)
    }
    
    private class func propertyItems(key:String) -> [Maqam] {
        
        var items = [Maqam]()
        if let plistArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] {
            
            for itemDict in plistArray {
                if let item = Maqam(fromDictionary: itemDict) {
                    items.append(item)
                }
            }
        }
        
        return items
    }
    
    private class func isMaqamIncludedInList(_ maqamId:Int, forKey key:String) -> Bool {
        
        if let plistArray = UserDefaults.standard.array(forKey: key) as? [[String:Any]] {
            
            return plistArray.contains(where: { (item) -> Bool in
                let iId = item["id"] as! Int
                return iId == maqamId
            })
        }
        
        return false
    }
    
    class func isMaqamTripIncluded(_ maqamId:Int) -> Bool {
        
        return self.isMaqamIncludedInList(maqamId, forKey: "tripItems")
    }
    
    class func isMaqamFavoritesIncluded(_ maqamId:Int) -> Bool {
        
        return self.isMaqamIncludedInList(maqamId, forKey: "favoriteItems")
    }
    
    class func addMaqamToTrip(_ maqam:Maqam) -> Void {
        
        addMaqamToList(maqam, key:"tripItems")
        VisitsManager.shared.updateMonitoredRegions()
    }
    
    class func removeMaqamFromTrip(_ maqam:Maqam) -> Void {
        
        removeMaqamFromList(maqam.id, key:"tripItems")
        VisitsManager.shared.updateMonitoredRegions()
    }
    
    class func removeMaqamFromTrip(id:Int) -> Void {
        
        removeMaqamFromList(id, key:"tripItems")
        VisitsManager.shared.updateMonitoredRegions()
    }
    
    class func tripsItems() -> [Maqam] {
        
        return propertyItems(key: "tripItems")
    }
    
    class func addMaqamToFavorite(_ maqam:Maqam) -> Void {
        
        addMaqamToList(maqam, key:"favoriteItems")
    }
    
    class func removeMaqamFromFavorite(_ maqam:Maqam) -> Void {
        
        removeMaqamFromList(maqam.id, key:"favoriteItems")
    }
    
    class func removeMaqamFromFavorite(id:Int) -> Void {
        
        removeMaqamFromList(id, key:"favoriteItems")
    }
    
    class func favoriteItems() -> [Maqam] {
        
        return propertyItems(key: "favoriteItems")
    }
    
    class func setMaqamVisited(_ maqamId:Int) {
        
        if var itemsArray = UserDefaults.standard.array(forKey: "tripItems") as? [[String:Any]] {
            
            for i in 0 ..< itemsArray.count {
                
                var itemDict = itemsArray[i]
                
                if let itemId = itemDict["id"] as? Int, itemId == maqamId {
                    itemDict["visited"] = true
                    itemsArray[i] = itemDict
                }
            }
            
            UserDefaults.standard.set(itemsArray, forKey: "tripItems")
        }
    }
}

