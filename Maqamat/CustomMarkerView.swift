//
//  CustomMarkerView.swift
//  maqamat
//
//  Created by AhmeDroid on 3/19/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import Foundation
import UIKit

class CustomMarkerView: UIView {
    
//    var image : [UIImage?] = [UIImage(named : "Maqam_Active") , UIImage(named : "Maqam_notReady") , UIImage(named : "Maqam_Ready And nearest")]
    var image : UIImage!
    
    
    
    func setupMarker () {
        let imgView = UIImageView (image: image)
        imgView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        imgView.layer.cornerRadius = 25
        imgView.clipsToBounds = true
        
        
        
    
    
    }
    
    
    
    
}
