//
//  MaqamDetailsViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/28/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import CoreLocation
import SVProgressHUD
import Alamofire
import AlamofireImage

enum CharacterType:Int {
    case companion = 2
    case prophet = 1
}

struct WeatherState {
   
    static let sunny = WeatherState(id: 1, text: "مشمس")
    static let cloudy = WeatherState(id: 2, text: "غائم")
    static let partlyCloudy = WeatherState(id: 3, text: "غيوم متفرقة")
    static let snowy = WeatherState(id: 4, text: "ثلوج")
    static let foggy = WeatherState(id: 5, text: "ضباب")
    static let windy = WeatherState(id: 6, text: "رياح قوية")
    static let unknown = WeatherState(id: 0, text: "غير معلوم")
    
    static let all:[WeatherState] = [.sunny, .cloudy, .partlyCloudy, .snowy, .foggy, .windy]
    static func state(forId id:Int) -> WeatherState {
        return self.all.first(where: { (state) -> Bool in
            return state.id == id
        }) ?? .unknown
    }
    
    var id:Int
    var text:String
}

class MaqamInfo: MapActivity {
    
    var id:Int
    var name:String
    var temperature:Int
    var weather:WeatherState
    var type:CharacterType
    var province:Province
    var coordinate:CLLocationCoordinate2D
    
    var aboutCharacter:String?
    var aboutMaqam:String?
    var photoUrls = [String]()
    var openDays:String?
    var openHours:String?
    var phoneNumber:String?
    
    init(id:Int, name:String, temperature:Int, weather:WeatherState, type:CharacterType, province:Province, coords:CLLocationCoordinate2D) {
        
        self.id = id
        self.name = name
        self.temperature = temperature
        self.weather = weather
        self.type = type
        self.province = province
        self.coordinate = coords
    }
    
    func createItem() -> Maqam {
        
        return Maqam(id: self.id, name: self.name, province: self.province, coords: self.coordinate)
    }
    
    var subtitle: String? {
        return self.province.name
    }
}

class MaqamDetailsViewController: UIViewController {
    
    var maqamInfo:MaqamInfo!
    var photos = [String:UIImage]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var openingTimeLabel: UILabel!
    @IBOutlet weak var openingDaysLabel: UILabel!
    @IBOutlet var roundedViews: [UIView]!
    
    @IBOutlet var photosCollectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet weak var tripLabel: UILabel!
    @IBOutlet weak var favoriteLabel: UILabel!
    @IBOutlet weak var tripButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var weatherStateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherDateLabel: UILabel!
    
    @IBOutlet var weatherView: UIView!
    @IBOutlet var opinionButton: UIButton!
    @IBOutlet var callUsButton: UIButton!
    
    @IBOutlet weak var aboutCharacterHeaderLabel: UILabel!
    @IBOutlet weak var collapseIcon_character: UIImageView!
    @IBOutlet weak var collapseIcon_maqam: UIImageView!
    
    @IBOutlet weak var aboutLabel_maqam: UILabel!
    @IBOutlet weak var aboutLabel_character: UILabel!
    @IBOutlet var characterHeight_const: NSLayoutConstraint!
    @IBOutlet var maqamHeight_const: NSLayoutConstraint!
    
    @IBOutlet weak var provinceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        let snapLayout = SnappingCollectionViewLayout()
        snapLayout.minimumLineSpacing = 30
        snapLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        snapLayout.itemSize = CGSize(width: self.view.frame.width - 30, height: 230)
        snapLayout.scrollDirection = .horizontal
        
        self.photosCollectionView.collectionViewLayout = snapLayout
        
        self.roundedViews.forEach { (view) in
            view.layer.cornerRadius = 4
            view.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0, height: 1), radius: 2)
        }
        
        self.weatherView.applyDarkShadow(opacity: 0.2,
                                         offset: CGSize(width: 0, height: 2),
                                         radius: 1)
        
        self.pageControl.hidesForSinglePage = true
        self.opinionButton.layer.cornerRadius = 4
        self.callUsButton.layer.cornerRadius = 4
        
        self.fillInMaqamInfo()
    }
    
    @IBAction func openRatingView(_ sender: Any) {
        
        RatingViewController.present(forMaqam: self.maqamInfo)
    }
    
    @IBAction func doContacting(_ sender: Any) {
        
        
    }
    
    func alterFavoriteState(_ isAdded:Bool) {
        
        let color = isAdded
            ? UIColor(valueRed: 136, green: 5, blue: 4, alpha: 1)
            : UIColor(valueRed: 112, green: 144, blue: 171, alpha: 1)
        
        self.favoriteButton.tintColor = color
        self.favoriteLabel.textColor = color
        self.favoriteLabel.text = isAdded ? "مفضلة" : "أضف للمفضلة"
    }
    
    func alterTripState(_ isAdded:Bool) {
        
        let color = isAdded
            ? UIColor(valueRed: 136, green: 5, blue: 4, alpha: 1)
            : UIColor(valueRed: 112, green: 144, blue: 171, alpha: 1)
        
        self.tripButton.tintColor = color
        self.tripLabel.textColor = color
        self.tripLabel.text = isAdded ? "مضافة للرحلة" : "أضف للرحلة"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.expandCollapseCharacter(true)
        self.expandCollapseMaqam(false)
    }
    
    func fillInMaqamInfo() -> Void {
        
        self.nameLabel.text = self.maqamInfo.name
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ar_JO")
        dateFormatter.dateFormat = "EEEE - dd-MM-yyyy"
        
        self.weatherDateLabel.text = dateFormatter.string(from: Date())
        self.weatherStateLabel.text = self.maqamInfo.weather.text
        self.temperatureLabel.text = "\(self.maqamInfo.temperature)°"
        self.aboutLabel_character.text = self.maqamInfo.aboutCharacter
        self.aboutLabel_maqam.text = self.maqamInfo.aboutMaqam
        
        self.provinceLabel.text = self.maqamInfo.province.name
        
        var distance = "??";
        if let loc = LocationService.shared.currentLocation {
            
            let dest = calculateDistance(
                loc1: self.maqamInfo.coordinate,
                loc2: loc)
            
            let numberFormat = NumberFormatter()
            numberFormat.maximumFractionDigits = 2
            numberFormat.locale = Locale(identifier: "ar_JO")
            
            let destStr = numberFormat.string(from: NSNumber(value: dest / 1000)) ?? "??"
            
            distance = "\(destStr) كيلومتراً"
        }
        
        self.distanceLabel.text = distance
        
        self.openingDaysLabel.text = self.maqamInfo.openDays
        self.openingTimeLabel.text = self.maqamInfo.openHours
        
        self.pageControl.numberOfPages = self.maqamInfo.photoUrls.count
        
        self.alterFavoriteState(UserAppData.isMaqamFavoritesIncluded(self.maqamInfo.id))
        self.alterTripState(UserAppData.isMaqamTripIncluded(self.maqamInfo.id))
        
        let charHeader = self.maqamInfo.type == .companion ? "عن الصحابي" : "عن النبي"
        self.aboutCharacterHeaderLabel.text = charHeader
    }
    
    @IBAction func openMaqamLocation(_ sender: Any) {
        
        print("Open maqam location view")
        GoogleMapViewController.present(forActivity: self.maqamInfo)
    }
    
    @IBAction func characterBoxWasTapped(_ sender: Any) {
        
        let expand = !self.characterExpanded
        
        if expand {
            
            self.expandCollapseCharacter(true)
            self.expandCollapseMaqam(false)
        } else {
            
            self.expandCollapseCharacter(expand)
        }
    }
    
    @IBAction func maqamBoxWasTapped(_ sender: Any) {
        
        let expand = !self.maqamExpanded
        
        if expand {
            
            self.expandCollapseMaqam(true)
            self.expandCollapseCharacter(false)
        } else {
            
            self.expandCollapseMaqam(expand)
        }
    }
    
    var characterExpanded:Bool = false
    var maqamExpanded:Bool = false
    func expandCollapseCharacter(_ expand:Bool ) -> Void {
        
        if expand {
            
            let lf = self.aboutLabel_character.frame
            let ss = self.aboutLabel_character.sizeThatFits(CGSize(width: lf.width, height: CGFloat.infinity))
            let fitHeight = lf.origin.y + ss.height + 15 + 20
            
            self.collapseIcon_character.image = #imageLiteral(resourceName: "Collapse")
            self.characterHeight_const.constant = fitHeight
            
        } else {
            
            self.collapseIcon_character.image = #imageLiteral(resourceName: "expand")
            self.characterHeight_const.constant = 55 + 20
        }
        
        self.characterExpanded = expand
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func expandCollapseMaqam(_ expand:Bool ) -> Void {
        
        if expand {
            
            let lf = self.aboutLabel_maqam.frame
            let ss = self.aboutLabel_maqam.sizeThatFits(CGSize(width: lf.width, height: CGFloat.infinity))
            let fitHeight = lf.origin.y + ss.height + 15 + 15
            
            self.collapseIcon_maqam.image = #imageLiteral(resourceName: "Collapse")
            self.maqamHeight_const.constant = fitHeight
            
        } else {
            
            self.collapseIcon_maqam.image = #imageLiteral(resourceName: "expand")
            self.maqamHeight_const.constant = 55 + 15
        }
        
        self.maqamExpanded = expand
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    deinit {
        self.photos.removeAll()
    }
    
    @IBAction func popUpDirections(_ sender: Any) {
        
        GoogleMapViewController.present(forActivity: self.maqamInfo )
    }
    
    @IBAction func addMaqamToFavorites(_ sender: Any) {
        
        let isIncluded = UserAppData.isMaqamFavoritesIncluded(self.maqamInfo.id)
        var message = "تم إضافة المكان إلى المفضلة"
        
        if isIncluded {
            
            UserAppData.removeMaqamFromFavorite(id: self.maqamInfo.id)
            message = "تم الحذف من المفضلة"
        } else {
            
            UserAppData.addMaqamToFavorite( self.maqamInfo.createItem() )
        }
        
        self.alterFavoriteState(!isIncluded)
        SVProgressHUD.setMaximumDismissTimeInterval(1)
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.showSuccess(withStatus: message)
    }
    
    @IBAction func addMaqamToTrips(_ sender: Any) {
        
        let isIncluded = UserAppData.isMaqamTripIncluded(self.maqamInfo.id)
        var message = "تم إضافة المكان إلى الرحلة"
        
        if isIncluded {
            
            UserAppData.removeMaqamFromTrip(id: self.maqamInfo.id)
            message = "تم الحذف من الرحلة"
            
        } else {
            
            UserAppData.addMaqamToTrip( self.maqamInfo.createItem() )
        }
        
        self.alterTripState(!isIncluded)
        SVProgressHUD.setMaximumDismissTimeInterval(1)
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.showSuccess(withStatus: message)
    }
    
    @IBAction func shareMaqam(_ sender: Any) {
        
        let activityVC = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension MaqamDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.maqamInfo.photoUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: "maqamPhotoCell", for: indexPath) as! MaqamPhotoCollectionCell
        let photoUrl = self.maqamInfo.photoUrls[indexPath.row]
        
        cell.contentView.layer.cornerRadius = 6.0
        cell.contentView.layer.masksToBounds = true
        
        if let photo = self.photos[photoUrl] {
            
            cell.photoView.image = photo
            cell.enlargeView.isHidden = false
            cell.loadingIndicator.stopAnimating()
            
        } else {
            
            cell.loadingIndicator.startAnimating()
            cell.enlargeView.isHidden = true
            
            Alamofire.request(photoUrl).responseImage(completionHandler: { (response) in
                
                cell.loadingIndicator.stopAnimating()
                if let image = response.result.value {
                    
                    let photo = image.af_imageAspectScaled(toFill: cell.frame.size)
                    self.photos[photoUrl] = photo
                    cell.photoView.image = photo
                    cell.enlargeView.isHidden = false
                    cell.layoutSubviews()
                }
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.openPhotoBrowserStartingWithIndex(indexPath.row)
    }
    
    func openPhotoBrowserStartingWithIndex(_ index:Int) -> Void {
        
        let photos = self.maqamInfo.photoUrls.map { (photoPath) -> SKPhoto in
            
            if let photo = self.photos[photoPath] {
                
                return SKPhoto.photoWithImage(photo)
                
            } else {
                
                return SKPhoto.photoWithImageURL( photoPath )
            }
        }
        
        SKPhotoBrowserOptions.displayBackAndForwardButton = false
        
        let browser = SKPhotoBrowser(photos: photos)
        browser.initializePageIndex(index)
        self.present(browser, animated: true, completion: nil)
    }
}
