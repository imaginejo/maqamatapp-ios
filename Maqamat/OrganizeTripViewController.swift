//
//  OrganizeTripViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/22/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit

class OrganizeTripViewController: UIViewController, TripItemViewDelegate, LocationServiceDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var stackView: UIStackView!
    var pathTrack:PathTrackView!
    
    var tripItems = [Maqam]()
    func calculateBounds() -> GMSCoordinateBounds {
        
        var bounds = GMSCoordinateBounds()
        
        for maqam in self.tripItems {
            bounds = bounds.includingCoordinate(maqam.coords)
        }
        
        if let loc = LocationService.shared.currentLocation {
            bounds = bounds.includingCoordinate(loc)
        }
        
        return bounds
    }

    @IBOutlet weak var searchView: UIView!
    var mapView:GMSMapView!
    
    private var gradient:CAGradientLayer!
    
    @IBOutlet weak var mapContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchView.applyDarkShadow(opacity: 0.15, offset: CGSize(width: 0, height: 3), radius: 1.5)
        
        // User location
        let userLocation = CLLocationCoordinate2D(latitude: 31.9460819, longitude: 35.9303704)
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.latitude, longitude: userLocation.longitude, zoom: 15.0)
        
        self.mapView = GMSMapView.map(withFrame: self.mapContainer.bounds, camera: camera)
        self.mapView.settings.setAllGesturesEnabled(false)
        self.mapView.preferredFrameRate = GMSFrameRate.powerSave
        self.mapView.isMyLocationEnabled = true
        self.mapView.delegate = self
        
        self.mapContainer.insertSubview(mapView, at: 0)
        self.mapView.autoresizingMask = [.flexibleTopMargin,
                                         .flexibleLeftMargin,
                                         .flexibleRightMargin,
                                         .flexibleBottomMargin,
                                         .flexibleWidth,
                                         .flexibleHeight];
        
        
        self.gradient = CAGradientLayer()
        self.gradient.frame = self.mapContainer.bounds
        self.gradient.colors = [ UIColor(valueRed: 248, green: 249, blue: 250, alpha: 0.05).cgColor,
                                 UIColor(valueRed: 248, green: 249, blue: 250, alpha: 1).cgColor ]
        self.gradient.locations = [0.4, 1]
        self.gradient.startPoint = CGPoint(x: 0, y: 0)
        self.gradient.endPoint = CGPoint(x: 0, y: 1)
        self.mapContainer.layer.addSublayer(self.gradient)
        
        let footer = UIView()
        footer.backgroundColor = UIColor.clear
        self.stackView.addArrangedSubview(footer)
        
        footer.snp.makeConstraints { (maker) in
            maker.height.equalTo(25)
        }
        
        self.pathTrack = PathTrackView(frame: CGRect.zero)
        self.pathTrack.numOfDestinations = self.tripItems.count
        self.stackView.addSubview(self.pathTrack)
        self.pathTrack.snp.makeConstraints { (maker) in
            
            let th = 110 as CGFloat
            let fy = self.mapContainer.bounds.height + th * 0.5
            let sView = self.stackView!
            
            maker.height.greaterThanOrEqualTo(0)
            maker.top.equalTo(sView).offset(fy).priority(700)
            maker.bottom.equalTo(sView).offset( -25 - th * 0.5 ).priority(700)
            maker.right.equalTo(sView).offset(0)
            maker.width.equalTo(40)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print("Please execute the followign !")
        marker.icon = UIImage(named: "Maqam_notReady")
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        
        if let bounds = self.lastBounds {
            let insets = UIEdgeInsets(top: 80, left: 30, bottom: 40, right: 30 )
            if let cameraPos = self.mapView.camera(for: bounds, insets: insets) {
                self.mapView.animate(to: cameraPos)
            }
        }
    }
    
    func sortTripItems(_ location:CLLocationCoordinate2D) -> Void {
        
        var _items = self.tripItems
        
        self.tripItems = _items.filter({ (maqam) -> Bool in
            return maqam.visited
        })
        
        _items = _items.filter({ (maqam) -> Bool in
            return !maqam.visited
        })
        
        var coords = [location]
        while _items.count > 0 {
            
            let last = coords.last!
            
            var dest = calculateDistance(loc1: _items[0].coords, loc2: last)
            var index = 0
            for i in 1 ..< _items.count {
                
                let _dest = calculateDistance(loc1: _items[i].coords, loc2: last)
                
                if _dest < dest {
                    index = i
                    dest = _dest
                }
            }
            
            let item = _items.remove(at: index)
            coords.append( item.coords )
            self.tripItems.append(item)
        }
    }
    
    func locationService(_ service: LocationService, didReceiveLocation location: CLLocationCoordinate2D) {
        
        self.sortTripItems(location)
        self.reloadItemsList()
        self.showTripPathOnMap()
    }
    
    func reloadItemsList() -> Void {
        
        for i in 0 ..< self.tripItems.count {
            
            let item = self.tripItems[i]
            let view = self.itemsViews[i]
            
            view.nameLabel.text = item.name
            view.governorateLabel.text = item.province.name
            view.distanceLabel.text = item.distanceString
        }
    }
    
    var itemsViews:[TripItemView] = []
    func updateItemsList() -> Void {
        
        self.mapView.clear()
        self.tripItems = UserAppData.tripsItems()
        self.itemsViews.forEach { (itemView) in
            itemView.snp.removeConstraints()
            self.stackView.removeArrangedSubview(itemView)
        }
        
        self.itemsViews.removeAll()
        let lastIndex = self.stackView.arrangedSubviews.count - 1
        
        for maqam in self.tripItems {
            
            let iconName = maqam.visited ? "Maqam_ReadyAndnearest" : "Maqam_Active"
            let marker = GMSMarker()
            marker.position = maqam.coords
            marker.title = ""
            marker.snippet = maqam.name
            marker.map = self.mapView
            marker.icon = UIImage(named: iconName)
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isTappable = true
            
            let itemView = TripItemView.create(frame: CGRect.zero)
            itemView.delegate = self
            
            let vi = lastIndex + self.itemsViews.count
            self.stackView.insertArrangedSubview(itemView, at: vi)
            self.itemsViews.append(itemView)
            
            itemView.snp.makeConstraints({ (maker) in
                
                maker.height.equalTo(110)
            })
        }
        
        if LocationService.shared.currentLocation == nil {
            LocationService.shared.delegate = self
            LocationService.shared.updateLocation()
        }
        
        self.updatePathTrack()
        self.reloadItemsList()
        self.showTripPathOnMap()
    }
    
    func updatePathTrack() -> Void {
        
        let visitedNum = self.tripItems.filter({ (maqam) -> Bool in
            return maqam.visited
        }).count
        
        self.pathTrack.progress = visitedNum
        self.pathTrack.numOfDestinations = self.tripItems.count
    }
    
    var lastBounds:GMSCoordinateBounds?
    
    func showTripPathOnMap() -> Void {
        
        if self.tripItems.count == 0 {
            return
        }
        
        let bounds = self.calculateBounds()
        let insets = UIEdgeInsets(top: 80, left: 30, bottom: 40, right: 30 )
        self.lastBounds = bounds
        
        if let cameraPos = self.mapView.camera(for: bounds, insets: insets) {
            self.mapView.animate(to: cameraPos)
        }
        
        let notVisitedCoords = self.tripItems.filter({ (maqam) -> Bool in
            return !maqam.visited
        }).map { (maqam) -> CLLocationCoordinate2D in
            return maqam.coords
        }
        
        var coords = [CLLocationCoordinate2D]()
        if let loc = LocationService.shared.currentLocation {
            coords.append(loc)
        }
        
        coords.append(contentsOf: notVisitedCoords)
        
        // Get directions
        DirectionsAPI.bestRoute(throughPoints: coords) { (_path) in
            
            if let path = _path {
                
                let polyline = GMSPolyline(path: path)
                polyline.strokeColor = Colors.routeBlue
                polyline.strokeWidth = 3
                polyline.map = self.mapView
                
                let bounds_path = GMSCoordinateBounds(path: path).includingBounds(bounds)
                self.lastBounds = bounds_path
                
                if let cameraPos = self.mapView.camera(for: bounds_path, insets: insets) {
                    self.mapView.animate(to: cameraPos)
                }
            }
        }
    }
    
    func tripItemDidRequireDetails(_ tripItem: TripItemView) {
        
        if let index = self.itemsViews.index(of: tripItem) {
            let maqam = self.tripItems[index]
            loadThenShowMaqamDetails(maqam.id)
        }
    }
    
    func tripItemDidRequireDeletion(_ tripItem: TripItemView) {
        
        if let index = self.itemsViews.index(of: tripItem) {
            
            self.stackView.removeArrangedSubview(tripItem)
            self.itemsViews.remove(at: index)
            
            let maqam = self.tripItems.remove(at: index)
            UserAppData.removeMaqamFromTrip(maqam)
            
            self.updateItemsList()
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradient.frame = self.mapContainer.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.updateItemsList()
    }
    
    @IBAction func openSearchView(_ sender: Any) {
        
        print("Open up searching maqamat !!")
        
        SearchViewController.presentOn(self)
    }
}


