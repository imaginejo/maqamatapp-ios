//
//  Constants.swift
//  IzwetnaApp
//
//  Created by AhmeDroid on 12/14/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit

struct NotificationNames {
    
    //Example, Push Notification launch option. See AppDelegate
    static let AppOpenedByTappingPushNotification = Notification.Name("AppOpenedByTappingPushNotification")
    static let OrganizeTripWasRequested = Notification.Name("OrganizeTripWasRequested")
}

struct APIKeys {
    
    static let Google = "AIzaSyBTiQXhLKD4K4CR0FRGplXNsvArpk0CNCI"
}

struct Colors {
    static let main = UIColor(red: 180/255.0, green: 149/255.0, blue: 79/255.0, alpha: 1)
    static let routeBlue = UIColor(valueRed: 0, green: 160, blue: 255, alpha: 1)
}
