//
//  DirectionsAPI.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import Alamofire
import Swift

class DirectionsAPI {
    
    class func bestRoute(throughPoints points:[CLLocationCoordinate2D], completion:@escaping (GMSPath?) -> Void ) -> Void {
        
        if points.count >= 2 {
            
            let fp = points.first!
            let ep = points.last!
        
            let url = "https://maps.googleapis.com/maps/api/directions/json"
            var params:[String:Any] = [
                "origin": "\(fp.latitude),\(fp.longitude)",
                "destination": "\(ep.latitude),\(ep.longitude)",
                "key": APIKeys.Google
            ]
            
            if points.count > 2 {
                
                var waypoints = [String]()
                for point in points[1 ..< (points.count - 1) ] {
                    waypoints.append("\(point.latitude),\(point.longitude)")
                }
                
                params["waypoints"] = "optimize:true|\(waypoints.joined(separator: "|"))"
            }
            
            Alamofire.request(url, method: .get, parameters: params)
                .responseSwiftyJSON(completionHandler: { (response) in
                
                    if let json = response.result.value {
                        
                        if let pathStr = json["routes"][0]["overview_polyline"]["points"].string {
                            completion(GMSPath(fromEncodedPath: pathStr))
                            return
                        }
                    }
                    
                    completion(nil)
            })
            
        }
    }
}
