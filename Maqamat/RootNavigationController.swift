//
//  RootNavigationController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/22/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {
    
    func gotoIntro() -> Void {
        
        if UserDefaults.standard.bool(forKey: "launchedOnce") {
            
            if let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "tabBarVC") {
                self.pushViewController(tabBarVC, animated: true);
            }
            
        } else {
            
            if let introVC = self.storyboard?.instantiateViewController(withIdentifier: "introVC") {
                self.pushViewController(introVC, animated: true)
            }
        }
    }
}
