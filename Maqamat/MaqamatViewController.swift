//
//  MaqamatViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class MaqamatViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    var snappingLayout = SnappingCollectionViewLayout()
    var countsMap:[Int:Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countsMap = Province.countsMap
        self.collectionView.collectionViewLayout = self.snappingLayout
        self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Province.reloadMaqamatCounts(completion: { (counts) in
            self.countsMap = counts
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.currentCell == nil {
            
            let index = IndexPath(item: 0, section: 0)
            let cell = self.collectionView.cellForItem(at: index) as! ProvinceCardCell
            self.didScrollToCell(cell, at: index)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let vf = self.view.frame
        let w = vf.width * 0.5
        let h = 1.3 * w
        let sl = (vf.width - w) / 2.0
        
        snappingLayout.itemSize = CGSize(width: w, height: h)
        snappingLayout.minimumLineSpacing = 50
        snappingLayout.minimumInteritemSpacing = 10000
        snappingLayout.sectionInset = UIEdgeInsets(top: 0, left: sl, bottom: 0, right: sl)
        snappingLayout.scrollDirection = .horizontal
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Province.all.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "provinceCardCell", for: indexPath) as! ProvinceCardCell
        
        cell.imageView.image = Province.all[indexPath.item].picture()
        
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.nameLabel.isHidden = true
        self.countLabel.isHidden = true
        self.currentCell?.collapse()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var cCell:UICollectionViewCell?
        for cell in self.collectionView.visibleCells {
            
            let x0 = self.collectionView.contentOffset.x
            let x1 = x0 + self.collectionView.frame.width
            let cx0 = cell.frame.minX
            let cx1 = cell.frame.maxX
            
            if cx0 >= x0 && cx1 <= x1 {
                cCell = cell
                break;
            }
        }
        
        if let _cell = cCell as? ProvinceCardCell,
            let index = self.collectionView.indexPath(for: _cell) {
            
            self.didScrollToCell(_cell, at: index)
        }
    }
    
    var currentCell:ProvinceCardCell?
    var currentIndex:IndexPath?
    func didScrollToCell(_ cell:ProvinceCardCell, at indexPath:IndexPath) -> Void {
        
        self.collectionView.bringSubview(toFront: cell)
        cell.expand()
        
        let province = Province.all[indexPath.item]
        
        self.nameLabel.isHidden = false
        self.countLabel.isHidden = false
        self.nameLabel.text = province.name
        
        if let count = self.countsMap?[province.id] {
            self.countLabel.text = "عدد المقامات (\(count))"
        } else {
            self.countLabel.text = ""
        }
        
        self.currentCell = cell
        self.currentIndex = indexPath
    }
    
    var selectedProvince:Province!
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let index = self.currentIndex, index == indexPath {
            
            self.selectedProvince = Province.all[indexPath.item]
            self.performSegue(withIdentifier: "show_province_maqamat", sender: nil)
        } else {
            
            let cell = collectionView.cellForItem(at: indexPath) as! ProvinceCardCell
            
            self.currentCell?.collapse()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.didScrollToCell(cell, at: indexPath)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let sId = segue.identifier, sId == "show_province_maqamat" {
            let listVC = segue.destination as! MaqamatListViewController
            listVC.currentProvince = self.selectedProvince
        }
    }
}

class SnappingCollectionViewLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        guard let collectionView = collectionView else {
            return super.targetContentOffset(
                forProposedContentOffset: proposedContentOffset,
                withScrollingVelocity: velocity)
        }
        
        let dOffset = (collectionView.frame.width - self.itemSize.width) / 2.0
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        let horizontalOffset = proposedContentOffset.x + dOffset
        
        let targetRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
        
        let layoutAttributesArray = super.layoutAttributesForElements(in: targetRect)
        layoutAttributesArray?.forEach({ (layoutAttributes) in
            
            let itemOffset = layoutAttributes.frame.origin.x
            if fabsf(Float(itemOffset - horizontalOffset)) < fabsf(Float(offsetAdjustment)) {
                offsetAdjustment = itemOffset - horizontalOffset
            }
        })
        
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
}
