//
//  Maqam.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import Alamofire

struct Province {
    
    static var countsMap = [Int:Int]()
    static func reloadMaqamatCounts(completion:(([Int:Int]) -> Void)? ) -> Void {
        
        if countsMap.count > 0 {
            
            completion?(countsMap)
        } else {
            
            Alamofire.request(APIEndpoint.maqamatCount).responseSwiftyJSON { (response) in
                
                if let json = response.result.value, let items = json["items"].array {
                    
                    countsMap.removeAll()
                    for item in items {
                        if let id = item["id"].int,
                            let count = item["count"].int {
                            countsMap[id] = count
                        }
                    }
                }
                
                completion?(countsMap)
            }
        }
    }
    
    var id:Int
    var name:String
    var pictureName:String
    
    func picture() -> UIImage? {
        return UIImage(named: self.pictureName)
    }
    
    static let amman = Province(id: 1, name: "عمان العاصمة", pictureName: "pn_amman")
    static let balqa = Province(id: 2, name: "البلقاء", pictureName: "pn_balqa")
    static let zarqa = Province(id: 3, name: "الزرقاء", pictureName: "pn_zarqa")
    static let madaba = Province(id: 4, name: "مادبا", pictureName: "pn_madaba")
    static let irbid = Province(id: 5, name: "إربد", pictureName: "pn_irbid")
    static let mafraq = Province(id: 6, name: "المفرق", pictureName: "pn_mafraq")
    static let jerash = Province(id: 7, name: "جرش", pictureName: "pn_jerash")
    static let ajloun = Province(id: 8, name: "عجلون", pictureName: "pn_ajloun")
    static let karak = Province(id: 9, name: "الكرك", pictureName: "pn_karak")
    static let tafilah = Province(id: 10, name: "الطفيلة", pictureName: "pn_tafilah")
    static let maan = Province(id: 11, name: "معان", pictureName: "pn_maan")
    static let aqaba = Province(id: 12, name: "العقبة", pictureName: "pn_aqaba")
    
    static let all:[Province] = [
        Province.amman,
        Province.mafraq,
        Province.ajloun,
        Province.jerash,
        Province.balqa,
        Province.karak,
        Province.tafilah,
        Province.madaba,
        Province.maan,
        Province.aqaba,
        Province.zarqa
    ]
    
    static func province(forId id:Int) -> Province? {
        return Province.all.first { (province) -> Bool in
            return province.id == id
        }
    }
}

class Maqam {
    
    class func parseMaqamData(_ json:JSON) -> Maqam? {
        
        if let id = json["id"].int,
            let name = json["name"].string,
            let latitude = json["latitude"].double,
            let longitude = json["longitude"].double,
            let provinceId = json["province"].int,
            let province =  Province.province(forId: provinceId) {
            
            let coords = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let maqam = Maqam(id: id, name: name, province: province, coords: coords)
            maqam.imageUrl = json["imageUrl"].string
            return maqam
        }
        
        return nil
    }
    
    var id:Int
    var name:String
    var province:Province
    var coords:CLLocationCoordinate2D
    var imageUrl:String?
    var visited:Bool = false
    
    var distance:Double {
        
        if let loc = LocationService.shared.currentLocation {
            return calculateDistance(loc1: loc, loc2: self.coords)
        }
        
        return 0
    }
    
    var distanceString:String {
        
        let numberFormat = NumberFormatter()
        numberFormat.maximumFractionDigits = 2
        numberFormat.locale = Locale(identifier: "ar_JO")
        
        return "\(numberFormat.string(from: NSNumber(value: self.distance / 1000) )! ) كيلومتراً"
    }
    
    init(id:Int, name:String, province:Province, coords:CLLocationCoordinate2D) {
        
        self.id = id
        self.name = name
        self.province = province
        self.coords = coords
    }
    
    convenience init?(fromDictionary dictionary:[String:Any]) {
        
        if let id = dictionary["id"] as? Int,
            let name = dictionary["name"] as? String,
            let provinceId = dictionary["province"] as? Int,
            let province = Province.province(forId: provinceId),
            let coords = dictionary["coords"] as? [String:Double] {
            
            let loc = CLLocationCoordinate2D(latitude: coords["lat"]!, longitude: coords["lon"]!)
            let image = (dictionary["imageUrl"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            self.init(id: id, name: name, province: province, coords: loc)
            self.imageUrl = (image.isEmpty) ? nil : image
            self.visited = dictionary["visited"] as? Bool ?? false
            
        } else {
            
            return nil
        }
    }
    
    func dictionary() -> [String:Any] {
        
        return [
            "id": self.id,
            "name": self.name,
            "province": self.province.id,
            "coords": [
                "lat": self.coords.latitude,
                "lon": self.coords.longitude,
            ],
            "imageUrl": self.imageUrl ?? "",
            "visited": self.visited
        ]
    }
}


func calculateDistance(loc1:CLLocationCoordinate2D, loc2:CLLocationCoordinate2D) -> CLLocationDistance {
    
    let dLoc1 = CLLocation(latitude: loc1.latitude, longitude: loc1.longitude)
    let dLoc2 = CLLocation(latitude: loc2.latitude, longitude: loc2.longitude)
    
    return dLoc1.distance(from: dLoc2)
}
