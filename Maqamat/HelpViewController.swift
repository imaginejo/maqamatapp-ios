//
//  HelpViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/26/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController, UICollectionViewDelegate , UICollectionViewDataSource {
    
    var listOfInstructions = [
        "الالتزام بتعليمات وأنظكة المدود التي وضعت من اجل سلامة الناس ووقايتهم من أخطار السيارات٫ ،،الحذر من مخالفتها؛ لما يسببه ذلك من وق،ع الحوادث الكثيرة" ,
        "الالتزام بتعليمات وأنظكة المدود التي وضعت من اجل سلامة الناس ووقايتهم من أخطار السيارات٫ ،،الحذر من مخالفتها؛ لما يسببه ذلك من وق،ع الحوادث الكثيرة",
        "الالتزام بتعليمات وأنظكة المدود التي وضعت من اجل سلامة الناس ووقايتهم من أخطار السيارات٫ ،،الحذر من مخالفتها؛ لما يسببه ذلك من وق،ع الحوادث الكثيرة",
        "الالتزام بتعليمات وأنظكة المدود التي وضعت من اجل سلامة الناس ووقايتهم من أخطار السيارات٫ ،،الحذر من مخالفتها؛ لما يسببه ذلك من وق،ع الحوادث الكثيرة",
        "الالتزام بتعليمات وأنظكة المدود التي وضعت من اجل سلامة الناس ووقايتهم من أخطار السيارات٫ ،،الحذر من مخالفتها؛ لما يسببه ذلك من وق،ع الحوادث الكثيرة"
    ]
    
    @IBOutlet var helpCollectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet weak var instructionsView: UIView!
    @IBOutlet var phoneViews: [UIView]!
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        let snapLayout = SnappingCollectionViewLayout()
        snapLayout.minimumLineSpacing = 40
        snapLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        snapLayout.itemSize = CGSize(width: self.view.frame.width - 70, height: 120)
        snapLayout.scrollDirection = .horizontal
        
        self.helpCollectionView.collectionViewLayout = snapLayout
        
        self.instructionsView.layer.cornerRadius = 4
        self.instructionsView.applyDarkShadow(opacity: 0.25, offset: CGSize(width: 0, height: 1), radius: 2.0)
        
        self.pageControl.hidesForSinglePage = true
        self.pageControl.currentPage = 0
        
        self.phoneViews.forEach { (view) in
            
            view.layer.cornerRadius = 4
            view.applyDarkShadow(opacity: 0.25, offset: CGSize(width: 0, height: 1), radius: 2.0)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfInstructions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = helpCollectionView.dequeueReusableCell(withReuseIdentifier: "instructionCell", for: indexPath ) as! InstructionsBoxCollectionViewCell
        cell.InstructionsLabel.text = listOfInstructions[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.pageControl.currentPage = indexPath.row
    }
    
    @IBAction func callPolicePressed(_ sender: Any) {
        
        if let url = URL(string: "tel://00962798157305") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func callEmergencypressed(_ sender: Any) {
        
        if let url = URL(string: "tel://00962798157305") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func callfireDopPressed(_ sender: Any) {
        
        if let url = URL(string: "tel://00962798157305") {
            UIApplication.shared.openURL(url)
        }
    }
}
