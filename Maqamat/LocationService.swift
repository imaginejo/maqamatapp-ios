//
//  LocationService.swift
//  Visit Jordan Bot
//
//  Created by AhmeDroid on 10/26/16.
//  Copyright © 2016 Imagine Technologies. All rights reserved.
//

import UIKit
import CoreLocation

enum AuthorizationLevel {
    case always
    case whenInUse
}

@objc protocol LocationServiceDelegate:class {
    @objc optional func locationService(_ service:LocationService, didReceiveLocation location:CLLocationCoordinate2D)
    @objc optional func locationService(_ service:LocationService, didReceiveAddress place:CLPlacemark)
    @objc optional func locationServiceFailedGettingLocation(_ service:LocationService)
    @objc optional func locationServiceFailedGettingAddress(_ service:LocationService)
}

class LocationService: NSObject, CLLocationManagerDelegate {
    
    weak var delegate:LocationServiceDelegate?
    static var shared:LocationService = LocationService()
    
    var authorizationLevel:AuthorizationLevel
    var locationManager:CLLocationManager
    private override init() {
        
        self.authorizationLevel = .always
        self.locationManager = CLLocationManager()
        
        super.init()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        print("Location Change Authorization !")
        
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            
            if authorizationLevel == .always {
                manager.requestAlwaysAuthorization()
            } else {
                manager.requestWhenInUseAuthorization()
            }
        
        } else if status == .authorizedAlways {
            
            self.locationManager.startUpdatingLocation()
            
        } else if status == .authorizedWhenInUse {
            
            if authorizationLevel == .always {
                manager.requestAlwaysAuthorization()
            } else {
                self.locationManager.startUpdatingLocation()
            }
            
        } else if status == .denied || status == .restricted {
            
            showErrorMessage("Access to location service was denied by you. Please give access to location services to enjoy the full of features of the app.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("Error in location service: \(error.localizedDescription)")
        self.delegate?.locationServiceFailedGettingLocation?(self)
    }
    
    var currentPlacemark:CLPlacemark?
    var currentLocation:CLLocationCoordinate2D?
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("Did recieve user location ...")
        manager.stopUpdatingLocation()
        
        if let location = locations.last {
            
            self.currentLocation = location.coordinate
            self.delegate?.locationService?(self, didReceiveLocation: location.coordinate)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarksData, error) in
                
                if error != nil {
                    
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    self.delegate?.locationServiceFailedGettingAddress?(self)
                    
                } else if let placemarks = placemarksData, placemarks.count > 0 {
                    
                    let pm = placemarks[0]
                    self.currentPlacemark = pm
                    
                    let loc:String = pm.locality ?? ""
                    let sloc:String = pm.subLocality ?? ""
                    print("User location reverse geocoded: \(loc), \(sloc)")
                    
                    self.delegate?.locationService?(self, didReceiveAddress: pm)
                    
                } else {
                    
                    print("Problem with the data received from geocoder")
                    self.delegate?.locationServiceFailedGettingAddress?(self)
                }
            })
        }
    }
    
    func updateLocation() -> Void {
        print("Updating user location ...")
        self.locationManager.startUpdatingLocation()
    }
}
