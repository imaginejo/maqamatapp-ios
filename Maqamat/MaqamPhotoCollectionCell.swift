//
//  maqamatPhotosCollectionViewCell.swift
//  maqamat
//
//  Created by AhmeDroid on 3/24/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class MaqamPhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var photoView: UIImageView!
    @IBOutlet weak var enlargeView: UIView!
    
}
