//
//  PhotsAlbumCollectionViewCell.swift
//  maqamat
//
//  Created by AhmeDroid on 3/24/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class PhotsAlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var photoImageView: UIImageView!
}
