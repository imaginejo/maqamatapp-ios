//
//  FavouriteTableViewCell.swift
//  maqamat
//
//  Created by AhmeDroid on 3/18/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class FavouriteTableViewCell: UITableViewCell {
    
    var shadowView:UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var governorateLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var imageOfFavPlaces: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.layer.masksToBounds = true
        self.containerView.layer.cornerRadius = 3
        
        self.shadowView = UIView()
        self.shadowView.backgroundColor = UIColor.gray;
        self.shadowView.layer.cornerRadius = 3
        self.shadowView.applyDarkShadow(opacity: 0.15, offset: CGSize(width: 0, height: 2), radius: 2)
        
        self.insertSubview(self.shadowView, at: 0)
        
        self.imageOfFavPlaces.layer.cornerRadius = 4;
        self.imageOfFavPlaces.layer.masksToBounds = true;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.shadowView.frame = self.containerView.frame
    }

}
