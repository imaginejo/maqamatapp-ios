//
//  APIEndPoint.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 4/1/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import Foundation
import Alamofire

enum APIEndpoint {
    case rating(maqamId:Int, rating:Int, comment:String)
    case maqamatCount
    case maqamat(province:Int?, keyword:String?, location:String?)
    case maqamDetails(maqamId:Int)
}

enum Environment {
    case production
    case testing
    
    var host:String {
        switch self {
        case .production:
            return "http://maqamat.com.10-0-0-4.mint.imagine.com.jo"
        case .testing:
            return "https://maqamat-mock-server.herokuapp.com"
        }
    }
}

extension APIEndpoint:URLRequestConvertible {
    
    var path:String {
        
        switch self {
        case .rating:
            return "MaqamRating.ashx"
        case .maqamatCount:
            return "MaqamatCount.ashx"
        case .maqamat:
            return "Maqamat.ashx"
        case .maqamDetails:
            return "MaqamDetails.ashx"
        }
    }
    
    var environment:Environment {
        return .production
    }
    
    var url:String {
        return "\(self.environment.host)/\(self.path)"
    }
    
    func asURLRequest() throws -> URLRequest {
        
        guard let _url = URL(string: self.url) else { throw AFError.invalidURL(url: self.url) }
        
        do {
            var request = try URLRequest(url: _url, method: self.method)
            request = try self.encoding.encode(request, with: self.parameters)
            return request
        } catch {
            throw error
        }
    }
    
    var method:HTTPMethod {
        
        switch self {
        case .rating:
            return .post
        default:
            return .get
        }
    }
    
    var encoding:ParameterEncoding {
        
        switch self {
        case .rating:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    var parameters:Parameters? {
        
        switch self {
        case let .rating(maqamId, rating, comment):
            
            return [
                "maqamId": maqamId,
                "rating": rating,
                "comment": comment,
                "token": UserDefaults.standard.string(forKey: "fcmToken") ?? "iOS_SIMULATOR"
            ]
            
        case .maqamatCount:
            
            return nil
            
        case let .maqamat(province, keyword, location):
            
            return [
                "province": province ?? NSNull(),
                "keyword": keyword ?? NSNull(),
                "location": location ?? NSNull()
            ]
            
        case .maqamDetails(let maqamId):
            
            return ["maqamId": maqamId]
        }
    }
}
