//
//  FavoritesViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/22/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import CoreLocation

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LocationServiceDelegate {
    
    var favoriteItems:[Maqam] = []

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
    }
    
    func locationService(_ service: LocationService, didReceiveLocation location: CLLocationCoordinate2D) {
        
        self.recalculateItems(location)
        self.tableView.reloadData()
    }
    
    func recalculateItems(_ location:CLLocationCoordinate2D) -> Void {
        
        self.favoriteItems.sort { (mqm1, mqm2) -> Bool in
            return mqm1.distance < mqm2.distance
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.favoriteItems = UserAppData.favoriteItems()
        
        if let location = LocationService.shared.currentLocation {
            
            self.recalculateItems(location)
            self.tableView.reloadData()
            
        } else {
            
            LocationService.shared.delegate = self
            LocationService.shared.updateLocation()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.favoriteItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteTableViewCell
        
        let maqam = self.favoriteItems[indexPath.row]
        
        cell.nameLabel.text = maqam.name
        cell.governorateLabel.text = maqam.province.name
        cell.distanceLabel.text = maqam.distanceString
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let maqam = self.favoriteItems.remove(at: indexPath.row)
            UserAppData.removeMaqamFromFavorite(maqam)
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let maqam = self.favoriteItems[indexPath.row]
        loadThenShowMaqamDetails(maqam.id)
    }
}
