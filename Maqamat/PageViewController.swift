//
//  PageViewController.swift
//  IzwetnaApp
//
//  Created by AhmeDroid on 12/17/17.
//  Copyright © 2017 Imagine Technologies. All rights reserved.
//

import UIKit

protocol PageViewControllerListener:class {
    
    func pageViewController(_ pageView:PageViewController, didSwitchToIndex index:Int) -> Void
}

class PageViewController: UIPageViewController, UIPageViewControllerDelegate {
    
    weak var listener:PageViewControllerListener?
    
    var _swipedEnabled = true
    var isSwipeEnabled:Bool {
        
        set {
            _swipedEnabled = newValue
            self.dataSource = (newValue) ? self : nil
        }
        get {
            return _swipedEnabled
        }
    }
    
    var initialIndex:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        if isSwipeEnabled {
            self.dataSource = self
        }
        
        var firstVC:UIViewController?
        if let index = self.initialIndex, index < self.orderedViewControllers.count {
            firstVC = self.orderedViewControllers[index]
        } else {
            firstVC = self.orderedViewControllers.first
        }
        
        if let _firstVC = firstVC {
            setViewControllers([_firstVC],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    var orderedViewControllers: [UIViewController]!
    
    private (set) var currentIndex:Int = 0
    private var pendingIndex:Int = -1
    
    func showPageOfIndex(_ index:Int ) -> Void {
        
        if index >= 0 && index < self.orderedViewControllers.count {
            
            let _direction:UIPageViewControllerNavigationDirection
            _direction = index > currentIndex ? .reverse : .forward
            
            currentIndex = index
            setViewControllers([self.orderedViewControllers[index]],
                               direction: _direction,
                               animated: true,
                               completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        if let first = pendingViewControllers.first,
            let index = self.orderedViewControllers.index(of: first) {
            pendingIndex = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        //print("   - Finished: \(finished)")
        //print("   - Completed: \(completed)")
        
        if completed {
            currentIndex = pendingIndex
            pendingIndex = -1
            self.listener?.pageViewController(self, didSwitchToIndex: currentIndex)
        }
    }
}


// MARK: UIPageViewControllerDataSource

extension PageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

