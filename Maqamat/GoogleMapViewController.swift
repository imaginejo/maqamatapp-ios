//
//  GoogleMapViewController.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 4/1/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

protocol MapActivity {
    
    var coordinate:CLLocationCoordinate2D { get }
    var name:String { get }
    var subtitle:String? { get }
}

class GoogleMapViewController: UIViewController, GMSMapViewDelegate {
    
    class func present(forActivity activity:MapActivity) -> Void {
        
        if let topVC = UIApplication.topViewController(),
            let mapVC = topVC.storyboard?.instantiateViewController(withIdentifier: "googleMapVC") as? GoogleMapViewController {
            
            mapVC.currentActivity = activity
            topVC.present(mapVC, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var externalLinksView: UIView!
    var currentActivity:MapActivity?
    
    
    @IBAction func showPlaceLocation(_ sender: AnyObject) {
        
        if let location = self.currentActivity?.coordinate {
            self.mapView.animate(toLocation: location)
        }
    }
    
    @IBAction func showUserLocation(_ sender: AnyObject) {
        
        if let location = LocationService.shared.currentLocation {
            
            self.mapView.animate(toLocation: location)
        }
    }
    
    @IBAction func openMapAppDirections(_ sender: AnyObject) {
        
        if let userLoc = LocationService.shared.currentLocation,
            let placeLoc = self.currentActivity?.coordinate {
            
            if !openGoogleMapAppForDirections(userLoc, loc2: placeLoc) {
                openAppleMapAppForDirections(placeLoc)
            }
            
        }else if let placeLoc = self.currentActivity?.coordinate {
            
            openAppleMapAppForDirections(placeLoc)
        }
    }
    
    @IBAction func openMapApp(_ sender: AnyObject) {
        
        if let location = self.currentActivity?.coordinate {
            
            if !openGoogleMapAppUsingLocation(location) {
                openAppleMapAppUsingLocation(location)
            }
        }
    }
    
    func openGoogleMapAppForDirections(_ loc1:CLLocationCoordinate2D, loc2:CLLocationCoordinate2D) -> Bool {
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            
            let startParam = "saddr=\(coordsString(loc1))"
            let destinationParam = "daddr=\(coordsString(loc2))"
            let modeParam = "directionsmode=driving"
            let fullPath = "comgooglemaps://?\(startParam)&\(destinationParam)&\(modeParam)"
            
            if let fullUrl = URL(string: fullPath){
                UIApplication.shared.openURL(fullUrl)
            }else{
                
                print("Corrupted URL for Google Maps");
            }
            
            return true
        } else {
            
            print("Can't use comgooglemaps://")
            return false
        }
    }
    
    func openGoogleMapAppUsingLocation(_ location:CLLocationCoordinate2D) -> Bool {
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            
            var placeName:String = self.currentActivity?.name ?? "Unknown Place"
            placeName = placeName.compressingWhiteSpaces()
            placeName = placeName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let pinParam = "q=\(placeName)"
            let centerParam = "center=\(coordsString(location))"
            let zoomParam = "zoom=15"
            let viewsParam = "views=traffic"
            
            let fullPath = "comgooglemaps://?\(pinParam)&\(centerParam)&\(zoomParam)&\(viewsParam)"
            
            if let fullUrl = URL(string: fullPath){
                UIApplication.shared.openURL(fullUrl)
            }else{
                
                print("Corrupted URL for Google Maps");
            }
            
            return true
        } else {
            
            print("Can't use comgooglemaps://")
            return false
        }
    }
    
    func openAppleMapAppForDirections(_ location:CLLocationCoordinate2D) -> Void {
        
        let coordinates = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let options = [
            MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.currentActivity?.name ?? "Unknown Place"
        mapItem.openInMaps(launchOptions: options)
    }
    
    func openAppleMapAppUsingLocation(_ location:CLLocationCoordinate2D) -> Void {
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.currentActivity?.name ?? "Unknown Place"
        mapItem.openInMaps(launchOptions: options)
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: {_ in})
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationService.shared.updateLocation()
        
        if let place = self.currentActivity {
            
            self.mapView.animate(toLocation: place.coordinate);
            
            let marker = GMSMarker()
            marker.position = place.coordinate
            marker.title = place.name
            marker.snippet = place.subtitle
            marker.map = self.mapView
            
            self.mapView.selectedMarker = marker
            
            if let userLocation = LocationService.shared.currentLocation {
                
                let marker = GMSMarker()
                marker.position = userLocation
                marker.title = "موقعك الحالي"
                marker.map = self.mapView
                
                DirectionsAPI.bestRoute(throughPoints: [userLocation, place.coordinate], completion: { (path) in
                    
                    if let _path = path {
                        let polyline = GMSPolyline(path: _path)
                        polyline.strokeWidth = 3.0
                        polyline.strokeColor = Colors.routeBlue
                        polyline.map = self.mapView
                    }
                })
                
            }
        }
    }
    
    private var mapView:GMSMapView!
    override func loadView() {
        super.loadView()
        
        let camera = GMSCameraPosition.camera(
            withLatitude: -33.86,
            longitude: 151.20,
            zoom: 15.0)
        
        self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.mapView.isMyLocationEnabled = true
        self.mapView.isIndoorEnabled = true
        self.mapView.delegate = self
        
        self.view.insertSubview(self.mapView, at: 0)
    }
}

func coordsString(_ coord:CLLocationCoordinate2D) -> String {
    return "\(coord.latitude),\(coord.longitude)"
}
