//
//  TripItemView.swift
//  Maqamat
//
//  Created by Ali Hajjaj on 3/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit


protocol TripItemViewDelegate:class {
    func tripItemDidRequireDetails(_ tripItem:TripItemView) -> Void
    func tripItemDidRequireDeletion(_ tripItem:TripItemView) -> Void
}
class TripItemView: UIView {
    
    class func create(frame:CGRect) -> TripItemView {
        
        let view = UIView.loadFromNibNamed("TripItemView") as! TripItemView
        view.frame = frame
        
        return view
    }
    
    weak var delegate:TripItemViewDelegate?
    
    @IBOutlet weak var container: UIView!
    var shadowView:UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var governorateLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.container.layer.masksToBounds = true
        self.container.layer.cornerRadius = 3
        
        self.shadowView = UIView()
        self.shadowView.backgroundColor = UIColor.gray;
        self.shadowView.layer.cornerRadius = 3
        self.shadowView.applyDarkShadow(opacity: 0.15, offset: CGSize(width: 0, height: 2), radius: 2)
        
        self.insertSubview(self.shadowView, at: 0)
        
        self.imageView.layer.cornerRadius = 4;
        self.imageView.layer.masksToBounds = true;
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.shadowView.frame = self.container.frame
    }
    
    @IBAction func moreWasTapped(_ sender: Any) {
        
        let sheetVC = UIAlertController(title: "مقامات الصحابة", message: nil, preferredStyle: .actionSheet)
        
        let details = UIAlertAction(title: "عرض التفاصيل", style: .default) { (action) in
            self.delegate?.tripItemDidRequireDetails(self)
        }
        
        let delete = UIAlertAction(title: "حذف", style: .default) { (action) in
            self.delegate?.tripItemDidRequireDeletion(self)
        }
        
        let cancel = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        
        sheetVC.addAction(details)
        sheetVC.addAction(delete)
        sheetVC.addAction(cancel)
        UIApplication.topViewController()?.present(sheetVC, animated: true, completion:nil)
    }
}
