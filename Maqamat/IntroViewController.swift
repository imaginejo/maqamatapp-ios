//
//  IntroViewController.swift
//  Maqamat
//
//  Created by Suhayb Ahmad on 6/25/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
    }

}
