//
//  LoadingViewController.swift
//  Maqamat
//
//  Created by Suhayb Ahmad on 5/17/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    var timer:Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.timer = Timer.scheduledTimer(timeInterval: 3.4, target: self, selector: #selector( gotoInto ), userInfo: nil, repeats: false)
    }
    
    func gotoInto() -> Void {
        
        self.timer?.invalidate()
        self.timer = nil
        
        if let navVC = self.navigationController as? RootNavigationController {
            navVC.gotoIntro()
        }
    }
}
