//
//  MainViewController.swift
//  maqamat
//
//  Created by AhmeDroid on 3/15/18.
//  Copyright © 2018 Imagine Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire

class MainViewController: UIViewController, CLLocationManagerDelegate ,  GMSMapViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate, LocationServiceDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var container_heightConst: NSLayoutConstraint!
    @IBOutlet var lowerViews: [UIView]!
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        
        SearchViewController.presentOn(self)
    }
    
    @IBAction func nearbyButtonTapped(_ sender: Any) {
        
        self.performSegue(withIdentifier: "show_nearby_maqamat", sender: nil)
    }
    
    @IBAction func visitInfoButtonTapped(_ sender: Any) {
        
        
    }
    
    @IBAction func organizeTripButtonTapped(_ sender: Any) {
        
        NotificationCenter.default.post(name: NotificationNames.OrganizeTripWasRequested, object: nil)
    }
    
    func userLocation() -> CLLocationCoordinate2D {
        return LocationService.shared.currentLocation
            ?? CLLocationCoordinate2D(latitude: 31.9722843, longitude: 35.9035571)
    }
    
    var maqamatList = [MainMaqamDisp]()
    var mapView:GMSMapView!
    var previousSelectedMarker : GMSMarker?
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var containerOfTheMap: UIView!
    @IBOutlet var maqamCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        let snapLayout = SnappingCollectionViewLayout()
        snapLayout.minimumLineSpacing = 10
        snapLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        snapLayout.itemSize = CGSize(width: self.view.frame.width - 30, height: 145)
        snapLayout.minimumLineSpacing = 10
        snapLayout.minimumInteritemSpacing = 10000
        snapLayout.scrollDirection = .horizontal
        
        self.maqamCollectionView.collectionViewLayout = snapLayout
        self.maqamCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        let tabBarHeight = self.tabBarController?.tabBar.frame.size.height ?? 0
        self.container_heightConst.constant = self.view.frame.height - tabBarHeight
        
        self.loadingIndicator.hidesWhenStopped = true
        self.loadingIndicator.stopAnimating()
        
        self.lowerViews.forEach { (view) in
            view.layer.cornerRadius = 4
            view.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0, height: 2), radius: 2)
        }
        
        self.searchView.layer.cornerRadius = 4
        self.searchView.applyDarkShadow(opacity: 0.2, offset: CGSize(width: 0, height: 2), radius: 4)
        
        self.setupMapView()
        self.setupRefreshControl()
        
        LocationService.shared.delegate = self
        LocationService.shared.updateLocation()
    }
    
    func setupRefreshControl() -> Void {
        
        refreshControl.addTarget(self, action: #selector( loadMaqamatFromServer ), for: .valueChanged)
        refreshControl.tintColor = UIColor.gray
        refreshControl.attributedTitle = NSAttributedString(string: "إعادة التحميل",
                                                            attributes: [NSForegroundColorAttributeName: UIColor.gray])
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            self.scrollView.refreshControl = refreshControl
        } else {
            self.scrollView.addSubview(refreshControl)
        }
    }
    
    func setupMapView() -> Void {
        
        let startLocation  =  CLLocation(latitude: 32.837468 , longitude: 35.847638)
        let camera = GMSCameraPosition.camera(withLatitude: startLocation.coordinate.latitude,
                                              longitude:startLocation.coordinate.longitude , zoom: 6.0 )
        
        let cf = self.containerOfTheMap.frame
        let r =  CGRect(x: 0, y: 0, width: cf.width, height: cf.height - 50)
        self.mapView = GMSMapView.map(withFrame: r , camera: camera)
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.tiltGestures = false
        self.mapView.settings.zoomGestures = true
        self.mapView.settings.rotateGestures = true
        self.mapView.settings.scrollGestures = false
        self.mapView.settings.consumesGesturesInView = false
        
        self.containerOfTheMap.insertSubview(self.mapView, at: 0 )
        self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight ]
        
        let gr = CGRect(x: 0, y: cf.height - 100, width: cf.width, height: 50)
        let gradView = GradientView(frame: gr)
        gradView.colors = [UIColor(valueRed: 255, green: 253, blue: 250, alpha: 0),
                           UIColor(valueRed: 255, green: 253, blue: 250, alpha: 1)]
        gradView.locations = [0.0, 1.0]
        gradView.start = CGPoint(x: 0, y: 0)
        gradView.end = CGPoint(x: 0, y: 1)
        
        self.containerOfTheMap.insertSubview(gradView, at: 1)
        gradView.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
    }
    
    func locationService(_ service: LocationService, didReceiveLocation location: CLLocationCoordinate2D) {
        
        self.loadMaqamatFromServer()
    }
    
    func locationServiceFailedGettingLocation(_ service: LocationService) {
        
        self.loadMaqamatFromServer()
    }
    
    func loadMaqamatFromServer() -> Void {
    
        let location = self.userLocation()
        let locStr = "\(location.latitude),\(location.longitude)"
        
        self.loadingIndicator.startAnimating()
        
        Alamofire.request(APIEndpoint.maqamat(province: nil, keyword: nil, location: locStr))
            .responseSwiftyJSON { (response) in
            
            self.loadingIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            
            if let json = response.result.value {
                
                if let items = json["items"].array {
                    
                    self.maqamatList.removeAll()
                    for item in items {
                        
                        if let maqam = Maqam.parseMaqamData(item) {
                            
                            self.maqamatList.append( MainMaqamDisp(maqam) )
                        }
                    }
                    
                    self.maqamCollectionView.reloadData()
                    self.refreshMap()
                }
                
            } else if let err = response.error {
                
                showErrorMessage(err.localizedDescription)
            }
        }
    }
    
    let activeIcon = UIImage(named: "Maqam_Active")
    let nearestIcon = UIImage(named: "Maqam_ReadyAndnearest")
    
    func refreshMap() -> Void {
        
        let location = self.userLocation()
        let maqamat = self.maqamatList.map ({ (disp) -> Maqam in
            
            return disp.maqam
            
        }).sorted(by: { (maqam1, maqam2)  -> Bool in
            
            let d1 = calculateDistance(loc1: location, loc2: maqam1.coords)
            let d2 = calculateDistance(loc1: location, loc2: maqam2.coords)
            return d1 < d2
        })
        
        if let nearest = maqamat.first {
            
            self.refreshMarkers(nearest)
            if let index = self.maqamatList.index(where: { (disp) -> Bool in return disp.maqam.id == nearest.id }) {
                let indexPath = IndexPath(item: index, section: 0)
                self.maqamCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
            
        } else {
            self.mapView.clear()
        }
        
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(location)
        
        maqamat.forEach { (maqam) in
            bounds = bounds.includingCoordinate(maqam.coords)
        }
        
        let insets = UIEdgeInsets(top: 120, left: 20, bottom: 130, right: 20)
        if let camera = self.mapView.camera(for: bounds, insets: insets) {
            self.mapView.animate(to: camera)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return maqamatList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = maqamCollectionView.dequeueReusableCell(withReuseIdentifier: "mainMaqamCell", for: indexPath) as! MainCollectionViewCell
        let disp = self.maqamatList[indexPath.row]
        cell.showMaqam(disp)
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let collectionFrame = self.maqamCollectionView.bounds
        for cell in self.maqamCollectionView.visibleCells {
            
            if collectionFrame.contains(cell.frame), let index = self.maqamCollectionView.indexPath(for: cell) {
                self.refreshMarkers(self.maqamatList[index.row].maqam)
                self.mapView.animate(toLocation: self.maqamatList[index.item].maqam.coords)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print("mark tapped")
        if let maqam = marker.userData as? Maqam {
            
            self.refreshMarkers(maqam)
            
            if let index = self.maqamatList.index(where: { (disp) -> Bool in return disp.maqam.id == maqam.id }) {
                
                let indexPath = IndexPath(item: index, section: 0)
                self.maqamCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                self.mapView.animate(toLocation: self.maqamatList[index].maqam.coords)
            }
        }
        
        return true
    }
    
    func refreshMarkers(_ selectedMaqam:Maqam) -> Void {
        
        self.mapView.clear()
        self.maqamatList.forEach { (disp) in
            
            let maqam = disp.maqam
            
            let marker = GMSMarker()
            marker.position = maqam.coords
            marker.map = self.mapView
            marker.icon = selectedMaqam.id == maqam.id ? self.nearestIcon : self.activeIcon
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isTappable = true
            marker.userData = maqam
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let sId = segue.identifier, sId == "show_nearby_maqamat" {
            let listVC = segue.destination as! MaqamatListViewController
            listVC.currentList = self.maqamatList.map({ (disp) -> Maqam in return disp.maqam })
            listVC.currentTitle = "المقامات القريبة مني"
        }
    }
    
    deinit {
        self.maqamatList.forEach { (disp) in
            disp.image = nil
        }
        self.maqamatList.removeAll()
    }
}

class MainMaqamDisp: MapActivity {
    
    var maqam:Maqam
    var image:UIImage?
    
    init(_ maqam:Maqam) {
        self.maqam = maqam
    }
    
    var coordinate: CLLocationCoordinate2D {
        return maqam.coords
    }
    
    var name: String {
        return maqam.name
    }
    
    var subtitle: String? {
        return maqam.province.name
    }
}
